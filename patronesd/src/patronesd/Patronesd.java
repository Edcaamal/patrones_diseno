/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronesd;

/**
 *
 * @author edgar
 */
public class Patronesd {
    
    private static void objetoTradicional(){
        objeto myobj = new objeto();
        myobj.setClave("001");
        myobj.setNombre("Borrador");
        
        System.out.println(myobj.toString());
        
        objeto mymsg = new objeto();
        mymsg.setClave("msg");
        mymsg.setNombre("Hola Mundo");
        
        System.out.println(mymsg.toString());        
    }
    
    
    private static void pSingleton(){
        // No es posible por que el constuctor es de ambito private
        // conexion cDb = new conexion();
        
        // Utilizar la forma de instancia
        conexion cDb = conexion.getInstancia();
        cDb.conectar();
        cDb.desconectar();
        
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        objetoTradicional();
        pSingleton();
        
        
    }
    
}
