/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronesd;

/**
 *
 * @author edgar
 */
public class objeto {
    private String clave;
    private String nombre;

    public objeto(String clave, String nombre) {
        this.clave = clave;
        this.nombre = nombre;
    }

    public objeto() {
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "objeto{" + "clave=" + clave + ", nombre=" + nombre + '}';
    }
    
    
}
