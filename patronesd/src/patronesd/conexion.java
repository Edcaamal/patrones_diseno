/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronesd;

/**
 *
 * @author edgar
 */
public class conexion {
    private static conexion instancia;
    
    private conexion(){
        
    }

    public static conexion getInstancia() {
        if (instancia == null){
            instancia = new conexion();
        }
        return instancia;
    }
    
    public void conectar(){
        System.out.println("Conexión a DB Exitoso ");
    }
    
    public void desconectar(){
        System.out.println("Desconectar de la DB");
         
    }
    
}
